package com.tsystems.javaschool.tasks.calculator;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    private Queue<Token> tokens;
    private LinkedList<Token> operStack;
    private Queue<Token> resultQueue;

    Calculator() {
        tokens = new LinkedList<Token>();
        operStack = new LinkedList<Token>();
        resultQueue = new LinkedList<Token>();
    }

    public static void main(String[] args) throws IOException, IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String statement = reader.readLine().replaceAll(" ", "");
        
        Calculator c = new Calculator();
        System.out.println(c.evaluate(statement));


    }

    private boolean toPrefixNotation(String source) {
        if(source == null){
            return false;
        }
        Token token;
        double decimal;
        int position = 0;
        while (position < source.length()) {
            token = new Token(TokenId.NUMBER, 0, '0');
            char currentSymbol = source.charAt(position);
            switch (currentSymbol) {
                case '+':
                case '-':
                case '*':
                case '/':
                case '(':
                case ')':
                    token.id = TokenId.OPERATOR;
                    token.operator = currentSymbol;
                    position++;
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '.':
                    token.id = TokenId.NUMBER;
                    token.value = 0;
                    if (currentSymbol != '.') {
                        while ((currentSymbol >= '0') && (currentSymbol <= '9')) {
                            token.value = token.value * 10 + (float) (currentSymbol - '0');
                            position++;
                            if (position < source.length())
                                currentSymbol = source.charAt(position);
                            else
                                break;
                        }
                    }
                    if (currentSymbol == '.') {
                        position++;
                        if (position < source.length()) {
                            currentSymbol = source.charAt(position);
                            if (!Character.isDigit(currentSymbol))
                                return false;
                        } else
                            break;
                        decimal = 1;
                        while (Character.isDigit(currentSymbol)) {
                            token.value += (float) (currentSymbol - '0') / (decimal *= 10);
                            position++;
                            if (position < source.length())
                                currentSymbol = source.charAt(position);
                            else
                                break;
                        }
                    }
                    break;
                default:
                    return false;
            }
            tokens.add(token);
        }
        return true;
    }

    int GetPriority(char c) {
        switch (c) {
            case '(':
                return 0;
            case ')':
                return 1;
            case '+':
            case '-':
                return 2;
            case '*':
            case '/':
                return 3;
        }
        return 0;
    }

    void DropOpers(int priority) {
        Token token = new Token(TokenId.NUMBER, 0, '0');
        while (!operStack.isEmpty() && GetPriority(operStack.peekFirst().operator) >= priority) {
            token = operStack.poll();
            resultQueue.add(token);
        }
    }

    boolean Parser() {
        PARSER_STATE state = PARSER_STATE.WAIT_PREFIX;
        int brackets = 0,
                prior = 0;
        Token token = new Token(TokenId.NUMBER, 0, '0');
        if (tokens.isEmpty())
            return false;
        while (state != PARSER_STATE.END) {
            if ((token = tokens.poll()) == null) {
                if ((state == PARSER_STATE.WAIT_SUFFIX) && (brackets == 0))
                    state = PARSER_STATE.DONE;
                else
                    return false;
            }
            switch (state) {
                case WAIT_PREFIX:
                    if (token.id == TokenId.NUMBER) {
                        resultQueue.add(token);
                        state = PARSER_STATE.WAIT_SUFFIX;
                    } else if (token.id == TokenId.OPERATOR) {
                        if (token.operator == '(') {
                            operStack.push(token);
                            brackets++;
                        } else
                            return false;
                    }
                    break;
                case WAIT_SUFFIX:
                    if (token.id == TokenId.OPERATOR) {
                        if (token.operator != ')') {
                            prior = GetPriority(token.operator);
                            DropOpers(prior);
                            operStack.push(token);
                            state = PARSER_STATE.WAIT_PREFIX;
                        } else if (token.operator == ')') {
                            DropOpers(GetPriority(token.operator));
                            token = operStack.poll();
                            state = PARSER_STATE.WAIT_SUFFIX;
                            brackets--;
                        }
                    } else
                        return false;
                    break;
                case DONE:
                    DropOpers(0);
                    if (operStack.isEmpty())
                        return true;
                    else
                        return false;
                case END:
                    break;
            }
        }
        return false;
    }

    public String evaluate(String statement) {
        if (!toPrefixNotation(statement))
            return null;
        if (Parser())
            return Calculate();
        else
            return null;
    }

    String Calculate() {
        Token token = new Token(TokenId.NUMBER, 0, '0'),
                X = new Token(TokenId.NUMBER, 0, '0'), Y = new Token(TokenId.NUMBER, 0, '0');
        LinkedList<Token> stack = new LinkedList<Token>();
        while ((token = resultQueue.poll()) != null) {
            if (token.id == TokenId.NUMBER)
                stack.push(token);
            else if (token.id == TokenId.OPERATOR) {
                Y = stack.poll();
                X = stack.poll();
                switch (token.operator) {
                    case '+':
                        X.value += Y.value;
                        break;
                    case '-':
                        X.value -= Y.value;
                        break;
                    case '*':
                        X.value *= Y.value;
                        break;
                    case '/':
                        if (Y.value < 0.00001 && Y.value > -0.00001)
                            return null;
                        X.value /= Y.value;
                        break;
                }
                stack.push(X);
            }
        }
        X = stack.poll();
        if (!stack.isEmpty())
            return null;
        X.value *= 10000;
        int round1 = (int) Math.round(X.value);
        float round2 = (float) round1 / 10000;
        if (round2 % 1 == 0) {
            return (int) round2 + "";
        }
        return round2 + "";
    }

    ;

    private enum TokenId {
        NUMBER, OPERATOR
    }

    enum PARSER_STATE {
        WAIT_PREFIX, WAIT_SUFFIX, DONE, END
    }

    class Token {
        private TokenId id;
        private float value;
        private char operator;

        Token(TokenId _tokenId, int _tokenValue, char _tokenCharacter) {
            id = _tokenId;
            value = _tokenValue;
            operator = _tokenCharacter;
        }
    }
}
    
    


