package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    int[][] pyramid;
    int height;
    int width;
        
    public int[][] buildPyramid(List<Integer> inputNumbers)throws CannotBuildPyramidException {
        // TODO : Implement your solution here
        if (inputNumbers.contains(null) || inputNumbers.size() > 100)
                throw new CannotBuildPyramidException();

            Collections.sort(inputNumbers);

            int size = inputNumbers.size();
            for (int i = 0; i < inputNumbers.size(); i++) {
                if (inputNumbers.get(i) != null) {
                    size-=i;
                    if (size != 0 && size > 0)
                        height++;
                }
                else throw new CannotBuildPyramidException();
            }

            int current = 0;
            width = height * 2 - 1;
            pyramid = new int[height][width];
            for (int i = 0; i < height; i++) {
                int k = height - i - 1;
                for (int l = 0; l <= i; l++) {
                    pyramid[i][k] = inputNumbers.get(current);
                    current++;
                    k+=2;
                }
            }
            return pyramid;
        }
    }
